/*const Discord = require('discord.js');
const TatoApi = require('./apis/tatolioApi');



//UTIL
function MakeQuerablePromise(promise) {
    // Don't modify any promise that has been already modified.
    if (promise.isResolved) return promise;

    // Set initial state
    var isPending = true;
    var isRejected = false;
    var isFulfilled = false;

    // Observe the promise, saving the fulfillment in a closure scope.
    var result = promise.then(
        function(v) {
            isFulfilled = true;
            isPending = false;
            return v; 
        }, 
        function(e) {
            isRejected = true;
            isPending = false;
            throw e; 
        }
    );

    result.isFulfilled = function() { return isFulfilled; };
    result.isPending = function() { return isPending; };
    result.isRejected = function() { return isRejected; };
    return result;
}

Bot.util.findChannelByName = function(channelName){
    return Bot.client.channels.find(ch => ch.name === channelName);
};
Bot.util.getMsgChunks = function(content){
    return content.toString().split(' ');
};
Bot.util.getMsgPrefix = function(content){
    const chunks = Bot.util.getMsgChunks(content);
    return chunks[0]
};
Bot.util.getMsgCommand = function(content){
    const chunks = Bot.util.getMsgChunks(content);
    return chunks[1]
};
Bot.util.getMsgParams = function(content, skip){
    var skip = skip || 0;
    const chunks = Bot.util.getMsgChunks(content);
    return chunks.slice(2+skip);
};
Bot.util.isCorrectPrefix = function(content){
    if(Bot.prefix === Bot.util.getMsgPrefix(content)){
        return true;
    }else{
        return false;
    }
};
Bot.util.removeHashtag = function(str){
    if(str.charAt(0) === '#'){
        return str.slice(1);
    }else{
        return str;
    }
}

Bot.util.createImgEmbed = function(url){
    return new Discord.RichEmbed().setImage(url);
}
Bot.util.firstMentionUser = function(msg){
    return msg.mentions.members.first();
}
Bot.util.isMentioned = function(msg){
    if(msg.mentions.members.first() !== undefined){
        if(msg.mentions.members.first().id === "525439950510882831"){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}
Bot.util.mentionHelp = function(msg){
    if(Bot.util.isMentioned(msg)){
        Bot.util.sendHere(msg,"You pinged me? Try ` "+Bot.prefix+" list ` to get my commands.");
    }else{
        return;
    }
}
Bot.util.getUserById = async function(userid){
    return await Bot.client.fetchUser(userid);
}
Bot.util.createMemberChannel = async function(msg){
    // Creates a channel owned by the author and saves it to the database
    const memberId = msg.author.id;
    const guild = msg.guild;
    const channelName = Bot.util.getMsgParams(msg.content)[0];
    const channelType = Bot.util.getMsgParams(msg.content)[1] || 'text';

    if(!msg.guild.channels.exists('name', channelName)){
        const resultChannel = await guild.createChannel(channelName,channelType);
        //TODO add channel to database
        TatoApi.updateChannel(resultChannel.id, {
            ownerId: memberId
        })
    }else{
        Util.sendHere(msg,"A channel with this name already exists");
    }
}
Bot.util.deleteMemberChannel = async function(msg){
    // Creates a channel owned by the author and saves it to the database
    const memberId = msg.author.id;
    const guild = msg.guild;
    const channel = Bot.util.getMsgParams(msg.content)[0];

    if(!msg.guild.channels.exists('name', channelName)){
        const resultChannel = await guild.createChannel(channelName,channelType);
        //TODO add channel to database
        TatoApi.updateChannel(resultChannel.id, {
            ownerId: memberId
        })
    }else{
        Util.sendHere(msg,"A channel with this name already exists");
    }
}
Bot.util.canPay = async function(userId,amount){
    console.log("Payment made: "+amount);
    // If amount is bigger or same than zero
    if(amount >= 0){
        // Positive payment
        console.log("Positive");
        return true;
    }else{
        // Negative payment
        console.log("Negative");
        const userdata = await TatoApi.findUserAndCreateIfNotThere(userId);
        if(userdata.data.currency>=amount){
            //Can pay
            return true;
        }else{
            //Can't pay
            return false;
        }
    }
}
Bot.util.pay = async function(userId,amount){
    if(await Bot.util.canPay(userId,amount)){
        return await MakeQuerablePromise(TatoApi.updateMember(userId,{$inc:{currency: amount}}));
    }else{
        return false;
    }
}
Bot.util.transact = async function(senderId,receiverId,amount){
    const send = await Bot.util.pay(senderId,-amount);
    const rece = await Bot.util.pay(receiverId,amount);

    if(send.isFulfilled()===true&&rece.isFulfilled()===true){
        return "Transaction succesful";
    }else{
        return "Woops transaction failed on one end and you might have lost money";
    }
}

module.exports = Bot.util;

*/