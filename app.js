const commando = require('discord.js-commando');
const sqlite = require('sqlite');
const oneLine = require('common-tags').oneLine;
const path = require('path');
const botData = require('./dataObjects/bot');

const TatoAPI = require('./apis/tatolioApi');

const client = new commando.Client({
    owner: '275688089584795659',
    commandPrefix:'>'
});

// SETTINGS DB
client.setProvider(
    sqlite.open(path.join(__dirname, 'settings.sqlite3')).then(db => new commando.SQLiteProvider(db))
).catch(console.error);

// REGISTERING COMMANDS
client.registry
.registerGroups([
    ['manage', 'Manage | Admin management commands'],
    ['games', 'Games | Game commands'],
    ['community', 'Community | Economy commands for server community'],
    ['informative', 'Informative | Tool commands for geeks'],
    ['diagnostic', 'Diagnostic | Status checking commands'],
    ['other', 'Other | Unimportant commands']
])
.registerDefaults()
// Registers all of your commands in the ./commands/ directory
.registerCommandsIn(path.join(__dirname, 'commands'));

//LISTENERS
client
    .on('ready', async() => {
		var obj = {
			guilds:[]
		};
		client.guilds.map((e)=>{
			var members = [];

			e.members.map(f=>{
				members.push({
					userId: f.id,
					userName: f.userName
				});
			})
			const confessChannel = e.channels.find(g=>{
				return g.name === "confessions";
			})
			obj.guilds.push({
				guildId: e.id,
				guildName: e.name,
				members: members,
				confessionChannel: confessChannel
			});
			
		});
		await TatoAPI.updateBot(obj);
		// transfer database data to local data when it restarts
		// transfer the guilds the bot is in to data when it restarts
		
		console.log(`Logged in as ${client.user.tag}!`);
    })
    .on('commandError', (cmd, err) => {
		if(err instanceof commando.FriendlyError) return;
		console.error(`Error in command ${cmd.groupID}:${cmd.memberName}`, err);
	})
	.on('commandBlocked', (msg, reason) => {
		console.log(oneLine`
			Command ${msg.command ? `${msg.command.groupID}:${msg.command.memberName}` : ''}
			blocked; ${reason}
		`);
	})
	.on('commandPrefixChange', (guild, prefix) => {
		console.log(oneLine`
			Prefix ${prefix === '' ? 'removed' : `changed to ${prefix || 'the default'}`}
			${guild ? `in guild ${guild.name} (${guild.id})` : 'globally'}.
		`);
	})
	.on('commandStatusChange', (guild, command, enabled) => {
		console.log(oneLine`
			Command ${command.groupID}:${command.memberName}
			${enabled ? 'enabled' : 'disabled'}
			${guild ? `in guild ${guild.name} (${guild.id})` : 'globally'}.
		`);
	})
	.on('groupStatusChange', (guild, group, enabled) => {
		console.log(oneLine`
			Group ${group.id}
			${enabled ? 'enabled' : 'disabled'}
			${guild ? `in guild ${guild.name} (${guild.id})` : 'globally'}.
		`);
	})
    .on('disconnect', () => { console.warn('Disconnected!'); })
	.on('reconnecting', () => { console.warn('Reconnecting...'); })
    .on('error', console.error)
    .on('warn', console.warn)
    .on('debug', console.log);

/*
// DIAGNOSE PROMISES
process.on('unhandledRejection', (reason, p) => {
    console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
});
*/
 
client.login('NTI1NDM5OTUwNTEwODgyODMx.Dv2rWQ.-boO6-2HEUHPHADoxGeW90et648');