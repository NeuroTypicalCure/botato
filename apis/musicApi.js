const ytdl = require("ytdl-core");

const Bot = require('../bot');

const meta = {
    name: "MusicApi(Youtube)"
};

function getName(){
    return meta.name;
}

function getOnlineStatus(){
    if(ytdl("https://www.youtube.com/watch?v=O2yPnnDfqpw", {filter:'audioonly'})){
        return "Online";
    }else{
        return "Offline";
    }
}

async function play(msg,url){
    try{
        const connection = await msg.member.voiceChannel.join();
        const streamOptions = { seek: 0, volume: 1 };
        const broadcast = Bot.client.createVoiceBroadcast();

        const stream = ytdl(url+"", { filter : 'audioonly' });
        broadcast.playStream(stream);
        meta.dispatcher = connection.playBroadcast(broadcast);

        meta.dispatcher.on('end', () => {
            // The song has finished
        });
        
        meta.dispatcher.on('error', e => {
            // Catch any errors that may arise
            console.log(e);
        });
    }catch(err){
        console.log(err);
    }
}

function resume(){
    if(meta.dispatcher){
        meta.dispatcher.resume();
    }else{
        console.log("there isn't any dispatcher");
    }   
}

function pause(){
    if(meta.dispatcher){
        meta.dispatcher.pause();
    }else{
        console.log("there isn't any dispatcher");
    }   
}

function stop(){
    if(meta.dispatcher){
        meta.dispatcher.end();
    }else{
        console.log("there isn't any dispatcher");
    }   
}


module.exports.getName = getName;
module.exports.getOnlineStatus = getOnlineStatus;
module.exports.play = play;
module.exports.resume = resume;
module.exports.pause = pause;
module.exports.stop = stop;