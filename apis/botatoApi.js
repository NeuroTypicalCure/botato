const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const botdbc = mongoose.createConnection("mongodb://174.138.12.128:27017", {
    dbName: 'botato',
    useNewUrlParser: true
});

botdbc.on('connected', function(){
    console.log("connected to db")
});
botdbc.on('error', function(){
    console.log("error with db connection")
});
botdbc.on('disconnected', function(){
    console.log("disconnected from db")
}); 

mongoose.Promise = global.Promise;

// Schemas
const botSchema = new Schema({
    botId:{type:String,default:"525439950510882831",unique:true},
    guilds: [{
        guildId:{type:String,unique:true},
        guildName:{type:String,unique:true},
        confessionChannel:{type:Object, default:null},
        members:[{
            userId:{type:String,unique:true},
            userName:String,
            reputation:{type:Number,default:0},
            currency:{type:Number,default:0},
            communityChannels:Array
        }]
    }]
});

module.exports = {
    BOT_ID: "525439950510882831",
    model: botdbc.model("Bot", botSchema)
}