const Axios = require('axios');
const Util = require('../util');

// http://api.mathjs.org/
const mathApi = Axios.create({
    baseURL: 'http://api.mathjs.org/v4/',
    timeout: 1000,
    headers: {'Content-Type': 'application/json'}
});

const meta = {
    name: "MathApi(Mathjs)"
};

function getName(){
    return meta.name;
}

async function getOnlineStatus(){
    var response = await mathApi.post('',{expr:"2+2"});

    if(response.data.result === "4"){
        return "Online";
    }
    return "Offline";
}

function sendResult(msg,expr){
    mathApi.post('',{expr:encodeURI(expr)})
    .then(function(response){
        if(response.data.error !== "null"){
            Util.sendHere(msg, response.data.result);
        }else{
            Util.sendHere(msg, response.data.error);
        }
    })
    .catch(err => console.log(err));
}

module.exports = {
    getName: getName,
    getOnlineStatus: getOnlineStatus,
    sendResult: sendResult
}

