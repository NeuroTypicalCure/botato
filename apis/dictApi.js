const Axios = require('axios');
const Util = require('../util');

// https://developer.oxforddictionaries.com/documentation
const dictApi = Axios.create({
    baseURL: 'https://od-api.oxforddictionaries.com/api/v1',
    timeout: 1000,
    headers: {
        "Accept": "application/json",
        "app_id": "73391945",
        "app_key": "a73a4773fb6e7da65f8775aa4bf8b20f"
    }
});

const meta = {
    name: "DictionaryApi(Oxford)"
};

function getName(){
    return meta.name;
}

async function getOnlineStatus(){
    var response = await dictApi.get('/entries/en/dog/definitions');
    console.log(response.data);
    return response.data.status;
}

async function sendDefinitions(msg,lemma){
    
    try{
        var response = await dictApi.get('/entries/en/'+ lemma +'/definitions');
        var lexicalCategory = "";
        var result = {
            word: response.data.results[0].id,
            count: 0,
            definitions: ""
        }

        for(var i=0;i<response.data.results.length;i++){
            for(var j=0;j<response.data.results[i].lexicalEntries.length;j++){
                lexicalCategory = response.data.results[i].lexicalEntries[j].lexicalCategory;
                for(var k=0;k<response.data.results[i].lexicalEntries[j].entries.length;k++){
                    for(var l=0;l<response.data.results[i].lexicalEntries[j].entries[k].senses.length;l++){
                        if(response.data.results[i].lexicalEntries[j].entries[k].senses[l].definitions !== undefined){
                            for(var n=0;n<response.data.results[i].lexicalEntries[j].entries[k].senses[l].definitions.length;n++){
                                result.definitions += result.count +": "+"["+lexicalCategory+"] "+ response.data.results[i].lexicalEntries[j].entries[k].senses[l].definitions[n] + "\n";
                                result.count++
                                if(response.data.results[i].lexicalEntries[j].entries[k].senses[l].subsenses !== undefined){
                                    for(var m=0;m<response.data.results[i].lexicalEntries[j].entries[k].senses[l].subsenses.length;m++){
                                        for(var o=0;o<response.data.results[i].lexicalEntries[j].entries[k].senses[l].subsenses[m].definitions.length;o++){
                                            result.definitions += result.count +": "+"["+lexicalCategory+"] "+ response.data.results[i].lexicalEntries[j].entries[k].senses[l].subsenses[m].definitions[o] + "\n";
                                            result.count++;
                                        }
                                    }
                                }else{
                                    continue;
                                }
                            }
                        }else{
                            continue;
                        }                
                    }
                }
            }
        }
        
        var embed = Util.createEmbed(result.word, result.definitions);
        Util.sendHere(msg, embed);
    }catch(err){
       console.log(err);
    }
}

module.exports = {
    getName: getName,
    getOnlineStatus: getOnlineStatus,
    sendDefinitions: sendDefinitions
}