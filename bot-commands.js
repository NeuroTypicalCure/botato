const Discord = require('discord.js');
const Bot = require('./bot');
const Util = require('./bot-util');

const TatoApi = require('./apis/tatolioApi');
const MathApi = require('./apis/mathApi');
const DictApi = require('./apis/dictApi');
const MusicApi = require('./apis/musicApi');

const tasks = require('./tasks.json');

const apis = [MathApi,DictApi,MusicApi];

//COMMANDS
Bot.commands.list = function(msg){
    var list = [];
    Object.keys(Bot.commands).forEach(element => {
        list.push(element);
    });
    Util.sendHere(msg,Util.createEmbed("Commands",list.join("\n")));
}


Bot.commands.message = function(msg){
    var channelname = Util.getMsgParams(msg.content)[0];
    var channel = Util.findChannelByName(Util.removeHashtag(channelname));
    channel.send(Util.getMsgParams(msg.content,1).join(' '));
}

Bot.commands.time = function(msg){
    console.log(msg.createdAt);
    
}
Bot.commands.purge = async function(msg){
    
}
Bot.commands.quote = async function(msg){
    var channel = msg.channel;
    var quoteMsgId = Util.getMsgParams(msg.content)[0];
    var quoteMsg = await channel.fetchMessage(quoteMsgId);

    Util.sendHere(msg,Util.createEmbed(quoteMsg.author.username,quoteMsg.content));
}
Bot.commands.embed = function(msg){
    var title = Util.getMsgParams(msg.content)[0];
    var description = Util.getMsgParams(msg.content)[1];

    Util.sendHere(msg,Util.createEmbed(title,description));
}
// Diagnostic commands
Bot.commands.checkapis = function(msg){
    var color;
    var status;

    for(var i=0;i<apis.length;i++){
        status = apis[i].getOnlineStatus();

        if(status === "Online"){
            color = "green";
        }else{
            color = "red";
        }

        Util.sendHere(msg,Util.createEmbed(apis[i].getName(), status).setColor(color));
    }
}
// Music commands
Bot.commands.play = function(msg){
    var url = Util.getMsgParams(msg.content)[0];
    MusicApi.play(msg,url);
}
Bot.commands.resume = function(){
    MusicApi.resume();
}
Bot.commands.pause = function(){
    MusicApi.pause();
}
Bot.commands.stop = function(){
    MusicApi.stop();
}
// Image commands
Bot.commands.kek = function(msg){
    Util.sendHere(msg,Util.createImgEmbed("https://i.imgur.com/Rw3AFRS.gif"));
}
Bot.commands.begone = function(msg){
    Util.sendHere(msg,Util.createImgEmbed("https://i.imgur.com/DgSxXvG.gif"));
}
Bot.commands.poke = function(msg){
    Util.sendHere(msg,Util.createImgEmbed("https://i.imgur.com/CRVOsh3.gif"));
}
// ECONOMY
Bot.commands.tasks = async function(msg){
    var result;
    var r = 0;
    var content = "";

    if(Util.getMsgParams(msg.content)[0] === "r"){
        r = Math.round(Math.random()*(tasks.length-1))
        result = "Random job: "+tasks[r].name+" | Reward: "+tasks[r].value+" tatoes";
    }else{
        for(var i=0;i<tasks.length;i++){
            content += i+": "+tasks[i].name+" | Reward: "+tasks[i].value+" tatoes"+"\n";
        }
        result = Util.createEmbed("Community jobs(optional)", content)
    }
    
    Util.sendHere(msg, result);
}
Bot.commands.bank = async function(msg){
    const author = msg.author;
    const mention = Util.firstMentionUser(msg);

    var target = author;
    if(mention !== undefined){
        target = mention;
    }

    const username = target.username;
    const id = target.id;

    const result = await TatoApi.findUserAndCreateIfNotThere(id);
    Util.sendHere(msg, Util.createEmbed(username+"\'s potato bank account", result.data.currency));    
}
Bot.commands.farm = async function(msg){
    const authorUserName = msg.author.username;
    const authorId = msg.author.id;
    const result = await Util.pay(authorId, Bot.prices.income);
    Util.sendHere(msg,Util.createEmbed(authorUserName+"\'s potato bank account", result.data.currency));
}
Bot.commands.cheat = async function(msg){
    if(msg.member.hasPermission("ADMINISTRATOR")){
        const authorUserName = msg.author.username;
        const authorId = msg.author.id;
        const amount = Util.getMsgParams(msg.content)[0];

        const result = await Util.pay(authorId, amount);
        Util.sendHere(msg,Util.createEmbed(authorUserName+"\'s potato bank account", result.data.currency));
    }else{
        Util.sendHere(msg,"You do not have the required permissions.")
    }
}
Bot.commands.send = async function(msg){
    const amount = Util.getMsgParams(msg.content)[0];
    const authorId = msg.author.id;
    const targetId = Util.firstMentionUser(msg).id;

    const transact = Util.transact(authorId,targetId,amount);
    Util.sendHere(msg,transact);
}
Bot.commands.buyhome = async function(msg){
    Util.createMemberChannel(msg);
    const price = -Bot.prices.home;
    Util.pay(msg.author.id,price);
    Util.sendHere(msg, "You just bought a home for: "+Bot.prices.home+" tatoes");
}


module.exports = Bot.commands;