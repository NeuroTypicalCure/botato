var botData = {
    botId:"525439950510882831",
    guilds: [],
    prices: {
        daily: 300
    }
}

function updateData(obj){
    botData = obj;
}

function getGuildById(id){
    return botData.guilds.find(e=> e.guildId === id);
}
function getGuildByName(guildName){
    return botData.guilds.find(e=> e.guildName === guildName);
}


module.exports = {
    updateData: updateData,
    getGuild: getGuildById,
    getGuildByName: getGuildByName
};