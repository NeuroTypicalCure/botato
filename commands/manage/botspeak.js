const cmdo = require('discord.js-commando');
const oneLine = require('common-tags').oneLine;

module.exports = class BotSpeak extends cmdo.Command{
    constructor(client){
        super(client,{
            name: 'botspeak',
            aliases: ['bs', 'bspeak'],
            group: 'manage',
            memberName: 'botspeak',
            description: 'Let the bot send a message for you',
            examples: ['botspeak #channel hello'],
            details: oneLine`
                Specify a channel and a message and the bot will send that message
                In the specified channel.
            `,
            args: [
            {
                key: 'channel',
                label:'channel',
                prompt: "What channel do you want me to send in?",
                type: 'channel',
            },
            {
                key: 'message',
                label:'message',
                prompt:'What message do you want to send?',
                type: 'string'
            }
            ],
            argsSingleQuotes:true,
            guildOnly:true,
            clientPermissions:['MANAGE_MESSAGES','READ_MESSAGE_HISTORY','SEND_MESSAGES','VIEW_CHANNEL'],
            userPermissions:['READ_MESSAGE_HISTORY','SEND_MESSAGES','VIEW_CHANNEL']
        })
    }

    async run(msg,args){
        const channel = args.channel;
        const message = args.message;
        
        channel.send(message);
        msg.delete();
    }
}