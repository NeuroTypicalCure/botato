const cmdo = require('discord.js-commando');
const oneLine = require('common-tags').oneLine;

module.exports = class Purge extends cmdo.Command{
    constructor(client){
        super(client,{
            name: 'purge',
            group: 'manage',
            memberName: 'purge',
            description: 'Deletes messages in a certain channel',
            examples: ['purge 3 5', "purge 50"],
            details: oneLine`
            Either deletes messages starting from the bottom, 
            or when using two numbers as arguments, 
            the first one is the amount of messages it skips before purging.
            `,
            args: [
            {
                key: 'start',
                label:'amount|start',
                prompt: "How many messages do you want to delete?",
                type: 'integer',
            },
            {
                key: 'amount',
                label:'0|amount',
                prompt:'',
                type: 'integer',
                default: 0
            }
            ],
            clientPermissions:['MANAGE_MESSAGES','READ_MESSAGE_HISTORY','SEND_MESSAGES','VIEW_CHANNEL'],
            userPermissions:['MANAGE_MESSAGES','READ_MESSAGE_HISTORY','SEND_MESSAGES','VIEW_CHANNEL'],
            guildOnly: true
        })
    }

    async run(msg,args){
        var start = args.start;
        var amount = args.amount;
        const channel = msg.channel;

        if(amount === undefined || amount === null || amount === 0){
            // Normal purge
            msg.channel.bulkDelete(start+1)
            .then(messages => console.log(`Bulk deleted ${messages.size} messages`))
            .catch(console.error);
        }else{
            // Sniper purge
            start = start;
            amount = amount;

            const msgsBefore = await channel.fetchMessages({limit:start+1});
            const lastBeforeId = msgsBefore.last().id;
            const msgsToDelete = await channel.fetchMessages({before:lastBeforeId,limit:amount});
            
            msg.delete();
            msg.channel.bulkDelete(msgsToDelete)
            .then(messages => console.log(`Bulk deleted ${messages.size} messages`))
            .catch(console.error);
        }
    }
}