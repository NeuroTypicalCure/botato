const cmdo = require('discord.js-commando');
const oneLine = require('common-tags').oneLine;

const BotData = require('../../dataObjects/bot');

module.exports = class Confess extends cmdo.Command{
    constructor(client){
        super(client,{
            name: 'confess',
            aliases: ['conf'],
            group: 'community',
            memberName: 'confess',
            description: 'Send an anonymous confession through the bot by DM\'ing.',
            examples: ['confess guild i did something bad'],
            details: oneLine`
                If your server admins have set a confession channel this
                command can be used to send anonymous messages from a dm to the bot.
            `,
            args: [
            {
                key: 'guildName',
                label:'guildName',
                prompt:'What is the name of the guild you wanna send to?',
                type: 'string'
            },
            {
                key: 'message',
                label:'message',
                prompt:'What message do you want to send?',
                type: 'string'
            }
            ],
            argsSingleQuotes:true,
            clientPermissions:['MANAGE_MESSAGES','SEND_MESSAGES','VIEW_CHANNEL'],
            userPermissions:['SEND_MESSAGES','VIEW_CHANNEL']
        })
    }

    async run(msg,args){
        const guild = BotData.getGuildByName(guildName);
        if(confessionChannel === null){
            console.log("no confession channel set, your guild needs to have a channel named \"confessions\"");
        }else{
            const message = args.message;
            guild.confessionChannel.send(message);
        }
        
    }
}