const cmdo = require('discord.js-commando');
const oneLine = require('common-tags').oneLine;

const Bot = require('../../dataObjects/bot');
const BotData = require('../../dataObjects/bot');
const TatoAPI = require('../../apis/tatolioApi');

module.exports = class Harvest extends cmdo.Command{
    constructor(client){
        super(client,{
            name: 'harvest',
            aliases: ['harvest','timely','daily','dailies','har'],
            group: 'community',
            memberName: 'harvest',
            description: 'harvest your daily potatoes',
            examples: ['harvest', 'timely', 'daily', 'dailies', 'har'],
            details: oneLine`
                Depending on the amount of waifus you have you'll be able to harvest an amount of potatoes from your farms
            `,
            clientPermissions:['SEND_MESSAGES','VIEW_CHANNEL'],
            userPermissions:['SEND_MESSAGES','VIEW_CHANNEL']
        })
    }

    async run(msg,args){
        const guildId = msg.guild.id;
        const authorId = msg.author.id;
        // get this person's daily modifier
        const modifier = 0;
        // get bot.prices.daily
        const daily = 300;
        
        TatoAPI.findAndModifyMember(guildId,authorId,{$inc:{'members.currency':daily*modifier}})
    }
}