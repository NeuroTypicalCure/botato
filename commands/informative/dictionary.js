const cmdo = require('discord.js-commando');
const oneLine = require('common-tags').oneLine;
const DictApi = require('../../apis/dictApi');

module.exports = class Dictionary extends cmdo.Command{
    constructor(client){
        super(client,{
            name: 'dictionary',
            aliases: ['dict'],
            group: 'informative',
            memberName: 'dictionary',
            description: 'Sends you the definition of a word',
            examples: ['dictionary dog', 'dict revelation'],
            details: oneLine`
                Uses the Oxford dictionary database to send you definitions. 
                https://oxforddictionaries.com for more info.
            `,
            args: [
            {
                key: 'word',
                label:'word',
                prompt:'What word do you want to get the definition of?',
                type: 'string'
            }
            ],
            clientPermissions:['SEND_MESSAGES','VIEW_CHANNEL'],
            userPermissions:['SEND_MESSAGES','VIEW_CHANNEL']
        })
    }

    async run(msg,args){
        DictApi.sendDefinitions(msg,args.word);
    }
}