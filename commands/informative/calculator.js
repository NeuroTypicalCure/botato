const cmdo = require('discord.js-commando');
const oneLine = require('common-tags').oneLine;
const MathApi = require('../../apis/mathApi');

module.exports = class Calculator extends cmdo.Command{
    constructor(client){
        super(client,{
            name: 'calculator',
            aliases: ['calc'],
            group: 'informative',
            memberName: 'calculator',
            description: 'Does math for you',
            examples: ['calculator 23*56', 'calc 399/6'],
            details: oneLine`
                Uses http://api.mathjs.org/, to calculate stuff for you.
            `,
            args: [
            {
                key: 'expression',
                label:'expression',
                prompt:'What do you want me to calculate?',
                type: 'string'
            }
            ],
            argsSingleQuotes:true,
            clientPermissions:['SEND_MESSAGES','VIEW_CHANNEL'],
            userPermissions:['SEND_MESSAGES','VIEW_CHANNEL']
        })
    }

    async run(msg,args){
        MathApi.sendResult(msg,args.expression);
    }
}