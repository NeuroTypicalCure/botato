const cmdo = require('discord.js-commando');
const oneLine = require('common-tags').oneLine;

module.exports = class MyTime extends cmdo.Command{
    constructor(client){
        super(client,{
            name: 'my-time',
            aliases: ['mtime'],
            group: 'informative',
            memberName: 'my-time',
            description: 'Send your local time in chat',
            examples: ['my-time','mtime'],
            details: oneLine`
                Sends the time you created this message and posts it in chat,
                so you can show people what time it is for you.
            `,
            guildOnly:true,
            clientPermissions:['SEND_MESSAGES','VIEW_CHANNEL'],
            userPermissions:['SEND_MESSAGES','VIEW_CHANNEL']
        })
    }

    async run(msg,args){
        msg.channel.send(msg.createdAt+"");
    }
}