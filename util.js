const Discord = require('discord.js');

function createEmbed(title,description){
    return new Discord.RichEmbed().setTitle(title).setDescription(description);
}

function sendHere(msg,resp){
    msg.channel.send(resp);
}

module.exports = {
    createEmbed: createEmbed,
    sendHere: sendHere
};